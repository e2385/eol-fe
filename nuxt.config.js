/*!

=========================================================
* Nuxt Argon Dashboard Laravel - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nuxt-argon-dashboard-laravel
* Copyright Creative Tim (https://www.creative-tim.com) & UPDIVISION (https://www.updivision.com)

* Coded by www.creative-tim.com & www.binarcode.com & www.updivision.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

const pkg = require("./package");
console.log("ENV", process.env.NODE_ENV);

module.exports = {
  publicRuntimeConfig: {
    baseURL:  process.NODE_ENV === "production" ? "https://elearning.com" : process.env.BASE_URL,
    apiURL: process.NODE_ENV === "production" ? "https://eol-dev.herokuapp.com/api/v1" : process.env.API_BASE_URL,
  },
  privateRuntimeConfig: {
  },
  env: {
    apiUrl: process.env.API_BASE_URL,
    baseUrl: process.env.BASE_URL,
    apiKey: process.env.API_KEY,
  },
  // mode: "spa",
  router: {
    base: "/",
    linkExactActiveClass: "active"
  },
  meta: {
    ogType: false,
    ogDescription: false,
    author: false,
    ogTitle: false,
    description: false,
    viewport: false,
    charset: false,
   },
  /*
   ** Headers of the page
   */
  head: {
    title: "Online English language school for children",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Nuxt Argon Dashboard Laravel comes with an API-powered Laravel backend, a Nuxt frontend and an awesome-looking Argon design."
      },
      {
        name: "keywords",
        content:
          "creative tim, updivision, html dashboard, nuxt, laravel, vue, vuejs, json:api, json, api, html css dashboard laravel, nuxt argon dashboard laravel, nuxt argon dashboard, argon admin, nuxt dashboard, nuxt admin, web dashboard, bootstrap 4 dashboard laravel, bootstrap 4, css3 dashboard, bootstrap 4 admin laravel, argon dashboard bootstrap 4 laravel, frontend, responsive bootstrap 4 dashboard, argon dashboard, argon laravel bootstrap 4 dashboard"
      },
      {
        itemprop: "name",
        content: "Nuxt Argon Dashboard Laravel by Creative Tim & UPDIVISION"
      },
      {
        itemprop: "description",
        content:
          "Nuxt Argon Dashboard Laravel comes with an API-powered Laravel backend, a Nuxt frontend and an awesome-looking Argon design."
      },
      {
        itemprop: "image",
        content:
          "https://s3.amazonaws.com/creativetim_bucket/products/350/original/opt_ad_nuxt_laravel_thumbnail.jpg"
      },
      {
        name: "twitter:card",
        content: "product"
      },
      {
        name: "twitter:site",
        content: "@creativetim"
      },
      {
        name: "twitter:title",
        content: "Nuxt Argon Dashboard Laravel by Creative Tim & UPDIVISION"
      },
      {
        name: "twitter:description",
        content:
          "Nuxt Argon Dashboard Laravel comes with an API-powered Laravel backend, a Nuxt frontend and an awesome-looking Argon design."
      },
      {
        name: "twitter:creator",
        content: "@creativetim"
      },
      {
        name: "twitter:image",
        content:
          "https://s3.amazonaws.com/creativetim_bucket/products/350/original/opt_ad_nuxt_laravel_thumbnail.jpg"
      },
      {
        property: "fb:app_id",
        content: "655968634437471"
      },
      {
        property: "og:title",
        content: "Nuxt Argon Dashboard Laravel by Creative Tim & UPDIVISION"
      },
      {
        property: "og:type",
        content: "article"
      },
      {
        property: "og:url",
        content:
          "https://www.creative-tim.com/live/nuxt-argon-dashboard-laravel"
      },
      {
        property: "og:image",
        content:
          "https://s3.amazonaws.com/creativetim_bucket/products/350/original/opt_ad_nuxt_laravel_thumbnail.jpg"
      },
      {
        property: "og:description",
        content:
          "Nuxt Argon Dashboard Laravel comes with an API-powered Laravel backend, a Nuxt frontend and an awesome-looking Argon design."
      },
      {
        property: "og:site_name",
        content: "Creative Tim"
      }
    ],
    link: [
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/images/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/images/favicons/favicon-16x16.png' },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.6.3/css/all.css",
        integrity:
          "sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/",
        crossorigin: "anonymous"
      },
      {
        rel: "stylesheet",
        href: "/vendor/laravel-h5p/css/laravel-h5p.css",
      },
      {
        rel: "stylesheet",
        href: "/vendor/h5p/h5p-core/styles/h5p.css",
      },
      {
        rel: "stylesheet",
        href: "/vendor/h5p/h5p-core/styles/h5p-confirmation-dialog.css",
      },
      {
        rel: "stylesheet",
        href: "/vendor/h5p/h5p-core/styles/h5p-core-button.css",
      },
      { rel: "stylesheet", href: "/assets/plugins/kipso-icons/style.css" },
      { rel: "stylesheet", href: "/assets/css/style.css" },
      { rel: "stylesheet", href: "/assets/css/responsive.css" },
    ],
    script: [
			{ src: '/vendor/h5p/h5p-core/js/jquery.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-event-dispatcher.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-x-api-event.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-x-api.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-content-type.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-confirmation-dialog.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/h5p-action-bar.js', body: true },
      { src: '/vendor/h5p/h5p-core/js/request-queue.js', body: true },
      { src: '/vendor/h5p/h5p-editor/scripts/h5peditor-editor.js', body: true },
      { src: '/vendor/laravel-h5p/js/laravel-h5p.js', body: true },
		]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/LoadingBar.vue',

  /*
   ** Global CSS
   */
  css: [
    "vue-phone-number-input/dist/vue-phone-number-input.css",
    "assets/css/nucleo/css/nucleo.css",
    "assets/sass/argon.scss",
    "assets/sass/home.scss",
    "~assets/css/style.css",
    // "~assets/css/datepicker.css",
    // "~assets/css/signup.css"
  ],

  router: {
    middleware: ['auth', 'markasread']
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/dashboard/dashboard-plugin",
    { src: "~/plugins/dashboard/world-map", ssr: false },
    { src: "~/plugins/dashboard/JsonApi.js", ssr: false },
    { src: '~plugins/bl-components', ssr: false },
    // "~/plugins/home/index.js",
    // "~/plugins/home/amplitude.js",
    '~/plugins/filters',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    "@nuxtjs/toast",
    '@nuxtjs/dotenv',
    // '@nuxtjs/proxy',
    'bootstrap-vue/nuxt',
    ['vue-social-sharing/nuxt', {
        networks: {
          fakeblock: 'https://fakeblock.com/share?url=@url&title=@title'
        }
      }],
  ],
  bootstrapVue: {
    // Install the `IconsPlugin` plugin (in addition to `BootstrapVue` plugin)
    icons: true
  },
  /*
	 ** Nuxt.js dev-modules
	 */
   buildModules: [
		// '@nuxt/typescript-build',
		// ['@nuxtjs/dotenv', { path: './', filename: '.env.' + process.env.ENV }]
		//['@nuxtjs/dotenv', { path: './', filename: '.env.local' }]
    ['@nuxtjs/laravel-echo', {
      broadcaster: 'pusher',
      authModule: true,
      connectOnLogin: true,
      disconnectOnLogout: true,
      authEndpoint: process.env.BACKEND_URL + '/broadcasting/auth',
      key: process.env.WEBSOCKET_KEY,
      cluster: process.env.WEBSOCKET_CLUSTER,
      // wsHost: process.env.WEBSOCKET_HOST,
      // wsPort: process.env.WEBSOCKET_PORT,
      encrypted: true,
      forceTLS: true,
      // disableStats: true,
    }],
	],
  echo: {
    plugins: [ '~/plugins/echo.js' ]
  },
  /*
   ** Auth module configuration
   ** See https://auth.nuxtjs.org/schemes/local.html#options
   */
  auth: {
    strategies: {
      local: {
        _scheme: "~/util/authCustomStrategy.js",
        endpoints: {
          login: {
            url: "login",
            method: "post",
            propertyName: "access_token"
          },
          logout: { url: "/logout", method: "post" },
          user: {
            url: "/me",
            method: "get",
            propertyName: false
          }
        }
      },
      google: {
        client_id: '469565628593-b4l44f1bb6pfeb9m9tt6qihovdfr46vn.apps.googleusercontent.com',
        redirect_uri: process.env.BASE_URL + '/oauth/google/callback',
        code_challenge_method: '',
        response_type: 'code',
        endpoints: {
          token: process.env.BACKEND_URL + '/user/google/', // somm backend url to resolve your auth with google and give you the token back
          userInfo: process.env.BACKEND_URL + '/auth/user/' // the endpoint to get the user info after you recived the token 
        },
      },
      redirect: {
        login: "/login",
        logout: "/",
        callback: "/callback",
        home: "/"
      }
    }
  },

  /*
   ** Notification toast module configuration
   ** See https://github.com/nuxt-community/modules/tree/master/packages/toast?ref=madewithvuejs.com
   */
  toast: {
    position: "top-right",
    duration: 5000,
    keepOnHover: true,
    fullWidth: false,
    fitToScreen: true,
    className: "vue-toast-custom",
    closeOnSwipe: true,
    register: [
      // Register custom toasts
      // @todo add custom messages as they come : login failed, register failed etc
      {
        name: "my-error",
        message: "Oops...Something went wrong",
        options: {
          type: "error"
        }
      }
    ]
  },

  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.API_BASE_URL,
    headers: {
      common: {
        Accept: "application/vnd.api+json",
        "content-type": "application/vnd.api+json"
      },
      post: {
        "content-type": "application/vnd.api+json"
      },
      patch: {
        "content-type": "application/vnd.api+json"
      },
      delete: {
        "content-type": "application/vnd.api+json"
      }
    },
    // proxy: true
  },

  // proxy: {
  //   // '/api/v1': { target: 'apiURL', pathRewrite: {'^/api/v1': ''}, changeOrigin: true }
  //   '/ajax': { target: 'apiURL', pathRewrite: {'^/ajax': ''}, changeOrigin: true }
  // },  

  /*
   ** Build configuration
   */
  build: {
    transpile: ["vee-validate/dist/rules"],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    presets({ isServer }) {
			const targets = isServer ? { node: 'current' } : { ie: 11 }
			return [[require.resolve('@babel/preset-env'), { targets }]]
		},
    extractCSS: process.env.NODE_ENV === "production",
    babel: {
      plugins: [
        [
          "component",
          {
            libraryName: "element-ui",
            styleLibraryName: "theme-chalk"
          }
        ]
      ]
    }
  },
  telemetry: false
};
