import _ from "lodash";
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en.json'
TimeAgo.addLocale(en)

const AVATAR_DEFAULT = "/img/placeholder.jpg";

const NOTIFICATION_TYPES = {
  FOLLOW: "App\\Notifications\\UserFollowed",
  NEWPOST: "App\\Notifications\\NewPost",
  COMMENT: "App\\Notifications\\CommentPost",
  GENERAL: "App\\Notifications\\GeneralNotification"
};

const TEMPLATE_NOTIFICATION =
'<div class="row align-items-center">'
  +'<div class="col-auto">'
    +'<img alt="avatar" src="${avatar}" class="avatar rounded-circle"/>'
  +'</div>'
  +'<div class="col ml--2">'
  +'<div class="d-flex justify-content-between align-items-center">'
  +'<div>'
  +'    <h4 class="mb-0 text-sm">${title}</h4>'
  +'  </div>'
  +'  <div class="text-right text-muted">'
  +'    <small>${timeAgo}</small>'
  +'  </div>'
  +'</div>'
  +'<p class="text-sm mb-0">'
  +'    ${content}'
  +'</p>'
  +'</div>'
  +'</div>';

export default {
  data() {
    return {
      TEMPLATE_NOTIFICATION,
      NOTIFICATION_TYPES,
    };
  },
  components: {
    TimeAgo,
  },
  methods: {
    // get the notification route based on it's type
    routeNotification(notification) {
      let to = "?read=" + notification.id;
      let slug = "";
      switch (notification.type) {
        case NOTIFICATION_TYPES.FOLLOW:
          to = "users" + to;
          break;
        case NOTIFICATION_TYPES.NEWPOST:
          slug = notification.data.post_slug;
          to = `post/${slug}` + to;
          break;
        case NOTIFICATION_TYPES.COMMENT:
          slug = notification.data.post_slug;
          to = `post/${slug}` + to;
          break;
        default:
          break;
      }
      return "/" + to;
    },
    // get the notification text based on it's type
    makeNotificationText(notification) {
      let text = "";
      let data = {};
      switch (notification.type) {
        case NOTIFICATION_TYPES.FOLLOW:
          let avatar =
            notification.data.follower_avatar || AVATAR_DEFAULT;
          let name = notification.data.follower_name;
          data = {
            title: "Followed",
            content:
              '<span class="font-weight-bold">' + name + "</span> followed you",
            avatar
          };
          break;
        case NOTIFICATION_TYPES.NEWPOST:
          let followingAvatar =
            notification.data.following_avatar || AVATAR_DEFAULT;
          let followingName = notification.data.following_name;
          data = {
            title: "Published a post",
            content: `<span class="font-weight-bold">${followingName}</span> published a post`,
            avatar: followingAvatar
          };
          break;
        case NOTIFICATION_TYPES.COMMENT:
          let userAvatar =
            notification.data.user_avatar || AVATAR_DEFAULT;
          let username = notification.data.user_name;
          data = {
            title: "",
            content: `<span class="font-weight-bold">${username}</span> commented on <span class="font-weight-bold">${notification.data.post_title} </span>`,
            avatar: userAvatar
          };
          break;
        case NOTIFICATION_TYPES.GENERAL:
            data = {
                title: notification.data.title,
                content: notification.data.content || `Hi ${notification.data.data.name} to our website!`,
                avatar: notification.data?.data?.avatar || AVATAR_DEFAULT
            };
            break;
        default:
          break;
      }
      data.to = this.routeNotification(notification);
      data.timeAgo = this.renderTimeAgo(notification);
      text = _.template(this.TEMPLATE_NOTIFICATION)(data);
      return text;
    },
    renderTimeAgo(notification) {
        let timeAgo = "just now";
        if (notification.created_at) {
            timeAgo = new TimeAgo('en-US').format(new Date(notification.created_at));
        }
        return timeAgo;
    }
  },
};
