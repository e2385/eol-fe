export default function({ app, redirect, route, $axios }) {
    const { read, ...params } = route.query;
    if ($axios && read) {
        $axios.get(`/notifications/${read}`);
        // unset the field read in the query string
    }
}