export default function({ app, redirect }) {
    if (app.$auth) {
        const user = app.$auth.user;
        if(user) {
            // admin or editor
            if(!(user.roles.some(role => ['editor', 'admin'].includes(role.name)))) {
                // redirect to / with error code 403
                return redirect('/?error=403&page=dashboard');
            }
        }
    }
}