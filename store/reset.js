export const actions = {
  async forgotPassword({ commit }, data) {
    await this.$axios.post("password-forgot", data);
  },
  async createNewPassword({ commit }, data) {
    await this.$axios.post("password-reset", data);

    const loginData = {
      data : {
        email: data.email,
        password: data.password
      }
    }

    this.$auth.loginWith("local", loginData);
  }
};
