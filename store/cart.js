import service from '../services/cart';

export const state = () => ({
  cart: [],
});

export const mutations = {
    SET_CART: (state, cart) => {
        state.cart = cart;
    },
    ADD_CART: (state, cart) => {
        state.cart.push(cart);
    },
    REMOVE_CART: (state, cart) => {
        state.cart.splice(cart, 1);
    },
    UPDATE_CART: (state, cart) => {
        state.cart.splice(cart, 1);
    }
};

export const actions = {
    getCart({commit, dispatch}, params) {
        return service.getCart(params, this.$axios)
            .then((cart) => {
                commit('SET_CART', cart);
            });
    },
    addToCart({commit, dispatch}, params) {
        return service.addToCart(params, this.$axios)
            .then((cart) => {
                dispatch('getCart');
            });
    },
    removeFromCart({commit, dispatch}, params) {
        return service.removeFromCart(params, this.$axios)
            .then((cart) => {
                dispatch('getCart');
            });
    }
};

export const getters = {
    cart: state => state.cart,
};
