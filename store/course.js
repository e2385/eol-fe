import service from "../services/course.js";

export const state = () => ({
  list: {},
  detail: {},
});

export const mutations = {
  SET_LIST: (state, list) => {
    state.list = list;
  },
  SET_DETAIL: (state, detail) => {
    state.detail = detail;
  },
};

export const actions = {
  getCourse({ commit, dispatch, state }, params) {
    // check if course is already in state
    if (state?.detail?.slug === params.slug && !params.force) {
        return Promise.resolve(state.detail);
    }
    return service.getCourse(params, this.$axios).then((data) => {
      commit("SET_DETAIL", data.data);
    });
  },
};

export const getters = {
  detail: (state) => state.detail,
};
