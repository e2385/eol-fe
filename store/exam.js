import service from '../services/exam';

export const state = () => ({
  questions: [],
  questionCurrent: null,
  questionCurrentIndex: 0,
  results: [],
});

export const mutations = {
//   SET_RESOURCE: (state, me) => {
//     state.me = me;
//   }
    SET_QUESTIONS: (state, questions) => {
        state.questions = questions;
    },
    SET_QUESTION_CURRENT: (state, questionCurrent) => {
        state.questionCurrent = questionCurrent;
    },
    SET_QUESTION_CURRENT_INDEX: (state, questionCurrentIndex) => {
        state.questionCurrentIndex = questionCurrentIndex;
    },
    SET_RESULTS: (state, results) => {
        state.results = results;
    }
};

export const actions = {
//   me({commit, dispatch}, params) {
//     return service.get(params, this.$axios)
//       .then((profile) => {
//         commit('SET_RESOURCE', profile.list);
//       });
//   },

//   update({commit, dispatch}, params) {
//     return service.update(params, this.$axios)
//       .then((profile) => {
//         commit('SET_RESOURCE', profile);
//       });
//   },
    questionCurrent({commit, dispatch}, params) {
        return service.questionCurrent(params, this.$axios)
            .then((questionCurrent) => {
                commit('SET_QUESTION_CURRENT', questionCurrent);
            });
    }
};

export const getters = {
  questions: state => state.questions,
  questionCurrent: state => state.questionCurrent,
  questionCurrentIndex: state => state.questionCurrentIndex,
  results: state => state.results
};
