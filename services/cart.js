const url = process.env.apiUrl;

function getCart(params, axios) {
  return axios.get(`${url}/cart`)
    .then(response => {
      return response.data.data;
    });
}

function addToCart(params, axios) {
    return axios.post(`${url}/cart`, params)
        .then(response => {
            return response.data.data;
        });
}

function removeFromCart(params, axios) {
    return axios.delete(`${url}/cart/${params.type}/${params.id}`)
        .then(response => {
            return response.data.data;
        });
}

function deleteCart(params, axios) {
    return axios.delete(`${url}/cart`)
        .then(response => {
            return response.data.data;
        });
}

export default {
    getCart,
    addToCart,
    removeFromCart,
    deleteCart
};
