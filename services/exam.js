const url = process.env.apiUrl;

function questionCurrent(params, axios) {
  return axios.get(`${url}/h5p/embed/${params.contentId}`)
    .then(response => {
      return response.data;
    });
}

export default {
    questionCurrent
};
