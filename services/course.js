const url = process.env.apiUrl;

function getCourse(params, axios) {
  return axios.get(`${url}/courses/slug/${params.slug}`)
    .then(response => {
      return response.data;
    });
}

export default {
    getCourse
};
