/* 
 *
 * @Project        
 * @Copyright      Djoudi
 * @Created        2018-02-20
 * @Filename       laravel-h5p-editor.js
 * @Description    
 *
 */
(function ($) {
    
    // setting for inside editor
    $.ajaxSetup({
        // headers: {
        //     'X-CSRF-TOKEN': window.parent.Laravel.csrfToken
        // },
        headers: {
            // "Accept": "application/json",
            // 'Access-Control-Allow-Origin': 'http://localhost:3000',
            // 'Content-Type': 'application/json',
            // 'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            // 'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        },
        dataType: 'json',
        // withCredentials: true,
    });

})(H5P.jQuery);
