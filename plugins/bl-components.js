import Vue from 'vue'
import DataTable from 'laravel-vue-datatable';
import OwlCarousel from 'v-owl-carousel';
import PortalVue from 'portal-vue'
import InfiniteScroller from '~/components/InfiniteScroller.vue';
import CkeditorNuxt from '@blowstack/ckeditor-nuxt';
import Multiselect from 'vue-multiselect';
import "vue-multiselect/dist/vue-multiselect.min.css";
import VueFeedbackReaction from 'vue-feedback-reaction';
import VueLoadmore from 'vuejs-loadmore';

Vue.use(VueLoadmore);
Vue.use(PortalVue);
Vue.use(DataTable);
Vue.component('carousel', OwlCarousel);
Vue.component('infinite-scroller', InfiniteScroller);
Vue.component('ckeditor-nuxt', CkeditorNuxt);
Vue.component('multiselect', Multiselect)
Vue.use(VueFeedbackReaction);
