import Vue from "vue";
import moment from "moment";

Vue.filter("formatPrice", (value) => {
    if (!value) return '$0.00';
  return value.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });
});

Vue.filter("formatRating", (value) => {
  let _rate = !value ? 0.0 : value.toFixed(1);
  return `${_rate}/5.0`;
});

Vue.filter("formatDate", (value, format = "DD-MMM-YYYY") => {
    if (value == 'Just now') return value;
    return moment(value).format(format);
});

Vue.filter("replaceAvatar", (value) => {
  if (value) {
    return value;
  }
  return "/img/placeholder.jpg";
});

Vue.filter("replacePostThumbnail", (value) => {
    if (value) {
        return value;
    }
    return "/img/post-default.jpg";
});
